The Benign Devil is a clown like bat. It is friendly in appearance except for its thoughtful wicked stare. It prefers lighter uplifting colors, but is happy in any attire.  Benign Devils soul purpose in life is to be the spokesperson for learning through lifes hard lessons.

### License
CC-BY-SA-4.0 - Creative Commons Attribution Share Alike 4.0
File: [LICENSE](LICENSE)

### Examples
![Benign Devil](assets/image/benign_devil.png)
![Benign Devil Inverted](assets/image/benign_devil_inv.png)

### Biography
The Benign Devil is a facilitator.  It came into existance from the love of peace and war.  At an early age BD was curious about communication and argument.  It grew up fast and hard and narrowed its desires into helping and sharing between others.  BD prefers direct respectful communication for all in the persuit of happiness and occasionaly some upfront and friendy trickory.  One of the most importat lessons BD has learned is, "to respect someone is to try and understand them selflessly".

BD's first job: SelfMadeHell.com (April 2017)
* Oversee selfmadehell.com
* Be a calm quit listener to all audiences on all subjects related to communication.
* Provide support and encuoragment to users so that they can speak freely about difficult situations.


### Contents
| Resource | Description |
| -------- | ----------- |
| Benign Devil Original Logo | Orange white colors; outline |
| Benign Devil inverted Original Logo | Inverted orange white color; outline |
| Benign Devil Favicon | Orange white colors; outline; various icons for mutliple systems |

### Changelog
* Version 1.0 - Benigh Devil was born with a bright orange outline and a white background.  BD is a little rough around the edges and could use some proper framing.

### Contributions
Liberal contribution friendly; Create merge request, describe why you want to MR.  MR is peer reviewed.


